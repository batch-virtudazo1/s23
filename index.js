let trainer = {
	name: "Ash Ketchum",
	age: 18,
	pokemon: ["Pikachu", "Charizzard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pickachu! I choose you!");
	}
}
console.log(trainer);
console.log("Result of Dot Notation:");
console.log(trainer.name);

console.log("Result of Dot Notation:");
console.log(trainer.pokemon);


trainer.talk();

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = level*3;
	this.attack = level*1.5;

	this.tackle = function(pokemon){
		
		console.log(this.name + " tackled " + pokemon.name);
		
		let damageReceived =  pokemon.health - this.attack;
		console.log(pokemon.name + "'s health is reduced to " + damageReceived);
		if(damageReceived <= 0){
			this.faint();
		}
	};

	this.faint = function(){
		console.log(pokemon.name + " has fainted!");
	};
}

let pokemon = new Pokemon("Pikcahu",12,24,12);
console.log(pokemon);

let pokemon2 = new Pokemon("Geodude",8,16,8);
console.log(pokemon2);

let pokemon3 = new Pokemon("Mewtwo",100,200,100);
console.log(pokemon3);

// pokemon2.tackle(pokemon3.name,pokemon3.attack);
pokemon3.tackle(pokemon);
//pokemon3.faint();
//pokemon.tackle(pokemon3);